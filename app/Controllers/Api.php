<?php namespace App\Controllers;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Api extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}

	//--------------------------------------------------------------------

    public function them_lead(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://comta.bitrix24.com/rest/1/89sa5tbzw7cqgv0j/crm.deal.add",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => array('TITLE' => 'API','TYPE_ID' => 'GOODS','STAGE_ID' => 'NEW','COMPANY_ID' => '1','CONTACT_ID' => '1','OPENED' => 'Y','ASSIGNED_BY_ID' => '1','PROBABILITY' => '30','CURRENCY_ID' => 'VND','OPPORTUNITY' => '1000'),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: multipart/form-data; boundary=--------------------------353888954532591344881123"
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;
        
      
    }
}
